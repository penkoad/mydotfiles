#!/bin/bash

sudo apt-get install aptitude vim exuberant-ctags tmux openssh-server curl
 # terminator

# copye dotfiles but the git ones. Avoid to set home directory as a project.
cp -rp .[^.g]* /home/$USER
# Still we want to benefit of our gitconfig customs
cp -p .gitconfig /home/$USER
cp -rp Scripts /home/$USER

sudo ln -s ~/Scripts/lsp /usr/local/bin/lsp

mkdir ~/bin && cd ~/bin && ln -s ~/Scripts/rmtemps

# Post install vim
sudo update-alternatives --config editor

# TODO

# Fontconfig

#    Download the latest version of PowerlineSymbols and the latest version of the fontconfig file.
#    Move PowerlineSymbols.otf to ~/.fonts/ (or another X font directory).
#    Run fc-cache -vf ~/.fonts to update your font cache.
#    Move 10-powerline-symbols.conf to either ~/.fonts.conf.d/ or ~/.config/fontconfig/conf.d/, depending on your fontconfig version.
 #   If you don’t see the arrow symbols, please close all instances of your terminal emulator or gvim. You may also have to restart X for the changes to take effect. If you still don’t see the arrow symbols, please submit an issue on GitHub.

# Patched font

#    Download the font of your choice from powerline-fonts. If you can’t find your preferred font in the powerline-fonts repo, you’ll have to patch your own font instead. See Font patching for instructions.
#    Move your patched font to ~/.fonts/ (or another X font directory).
#    Run fc-cache -vf ~/.fonts to update your font cache.
#    Update Gvim or your terminal emulator to use the patched font. (the correct font usually ends with for Powerline).
# If you don’t see the arrow symbols, please close all instances of your terminal emulator or gvim. You may also have to restart X for the changes to take effect. If you still don’t see the arrow symbols, please submit an issue on GitHub.


